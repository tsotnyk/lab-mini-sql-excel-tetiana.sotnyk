package com.j2core.sts.minisqlexcel.dao;

import com.j2core.sts.minisqlexcel.dto.CellDTO;
import com.j2core.sts.minisqlexcel.util.CloseableHibernateSession;
import com.j2core.sts.minisqlexcel.util.DBSessionProvider;
import org.hibernate.Transaction;
import org.testng.Assert;
import org.testng.annotations.*;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 9/29/16.
 */
public class HibernateWorkerDAOTest {


    @BeforeClass
    public void createFirstData(){

        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            List<CellDTO> setCollection = new LinkedList<>();
            setCollection.add(new CellDTO(1, 1, "5"));
            setCollection.add(new CellDTO(1, 2, "5"));
            setCollection.add(new CellDTO(1, 3, "5"));
            setCollection.add(new CellDTO(2, 1, "55"));
            setCollection.add(new CellDTO(2, 2, "55"));

            transaction = closeableSession.getSession().beginTransaction();

            for (CellDTO cell : setCollection){

                closeableSession.getSession().save(cell);

            }

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @DataProvider(name = "getCellValuePositiveCase")
    public Object[][] dataProviderGetCellValuePositiveCase(){
        return new Object[][]{
                {1, 1, "5"},
                {2, 2, "55"},
        };
    }


    @Test(dataProvider = "getCellValuePositiveCase")
    public void testGetCellValuePositiveCase(int rowId, int columnId, String value) throws SQLException {

        CellDTO cell = new CellDTO(rowId, columnId, value);
        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getCellValue(rowId, columnId);

        Assert.assertEquals(result.get(0), cell);

    }


    @DataProvider(name = "getCellValueNegativeCase")
    public Object[][] dataProviderGetCellValueNegativeCase(){
        return new Object[][]{
                {1, 5},
                {10, 2},
        };
    }


    @Test(dataProvider = "getCellValueNegativeCase")
    public void testGetCellValueNegativeCase(int rowId, int columnId) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getCellValue(rowId, columnId);

        Assert.assertTrue(result.isEmpty());

    }



    @DataProvider(name = "getRowPositiveCase")
    public Object[][] dataProviderGetRowPositiveCase(){
        return new Object[][]{
                {1, 3},
                {2, 2},
        };
    }


    @Test(dataProvider = "getRowPositiveCase")
    public void testGetRowPositiveCase(int rowId, int amountColumn) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getRow(rowId);

        Assert.assertEquals(result.size(), amountColumn);

    }


    @DataProvider(name = "getRowNegativeCase")
    public Object[][] dataProviderGetRowNegativeCase(){
        return new Object[][]{
                {5},
                {10},
        };
    }


    @Test(dataProvider = "getRowNegativeCase")
    public void testGetRowNegativeCase(int rowId) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getRow(rowId);

        Assert.assertTrue(result.isEmpty());

    }


    @DataProvider(name = "getColumnPositiveCase")
    public Object[][] dataProviderGetColumnPositiveCase(){
        return new Object[][]{
                {1, 2},
                {2, 2},
        };
    }


    @Test(dataProvider = "getColumnPositiveCase")
    public void testGetColumnPositiveCase(int columnId, int amountRow) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getColumn(columnId);

        Assert.assertEquals(result.size(), amountRow);

    }


    @DataProvider(name = "getColumnNegativeCase")
    public Object[][] dataProviderGetColumnNegativeCase(){
        return new Object[][]{
                {5},
                {10},
        };
    }


    @Test(dataProvider = "getColumnNegativeCase")
    public void testGetColumnNegativeCase(int rowId) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.getRow(rowId);

        Assert.assertTrue(result.isEmpty());

    }


    @DataProvider(name = "createCellTrue")
    public Object[][] dataProviderCreateCellTrue(){
        return new Object[][]{
                {45, 50, "333"},
                {46, 60, "4444"},
        };
    }


    @Test(dataProvider = "createCellTrue")
    public void testCreateCellNotException(int rowId, int columnId, String value) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertTrue(hibernateWorker.createCell(rowId, columnId, value));

    }


    @DataProvider(name = "createCell")
    public Object[][] dataProviderCreateCell(){
        return new Object[][]{
                {47, 50, "333"},
                {58, 60, "4444"},
        };
    }


    @Test(dataProvider = "createCell")
    public void testCreateCell(int rowId, int columnId, String value) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();
        CellDTO cell = new CellDTO(rowId, columnId, value);

        hibernateWorker.createCell(rowId, columnId, value);

        Assert.assertEquals(hibernateWorker.getCellValue(rowId, columnId).get(0), cell);

    }


    @DataProvider(name = "verifyRowId")
    public Object[][] dataProviderVerifyRowId(){
        return new Object[][]{
                {1, true},
                {2, true},
                {8, false}
        };
    }


    @Test(dataProvider = "verifyRowId")
    public void testVerifyRowId(int rowId, boolean result) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertEquals(hibernateWorker.verifyRowId(rowId), result);

    }


    @DataProvider(name = "verifyCell")
    public Object[][] dataProviderVerifyCell(){
        return new Object[][]{
                {1, 1, true},
                {2, 2, true},
                {5, 5, false},
                {1, 5, false}
        };
    }


    @Test(dataProvider = "verifyCell")
    public void testVerifyCell(int rowId, int columnId, boolean result) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertEquals(hibernateWorker.verifyCell(rowId, columnId), result);

    }



    @DataProvider(name = "fineValuePositiveCase")
    public Object[][] dataProviderFineValuePositiveCase(){
        return new Object[][]{
                {"5", 3},
                {"55", 2},
        };
    }


    @Test(dataProvider = "fineValuePositiveCase")
    public void testFineValuePositiveCase(String value, int amountCell) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.findValue(value);

        Assert.assertEquals(result.size(), amountCell);

    }


    @DataProvider(name = "fineValueNegativeCase")
    public Object[][] dataProviderFineValueNegativeCase(){
        return new Object[][]{
                {"value"},
                {"12345"},
        };
    }


    @Test(dataProvider = "fineValueNegativeCase")
    public void testFineValueNegativeCase(String value) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        List<CellDTO> result = hibernateWorker.findValue(value);

        Assert.assertTrue(result.isEmpty());

    }


    @BeforeGroups(groups = "delete")
    public void createNewRowsForDelete(){

        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            List<CellDTO> removeCollection = new LinkedList<>();
            removeCollection.add(new CellDTO(11, 10, "10"));
            removeCollection.add(new CellDTO(11, 11, "11"));
            removeCollection.add(new CellDTO(11, 12, "12"));
            removeCollection.add(new CellDTO(12, 10, "10"));
            removeCollection.add(new CellDTO(12, 11, "11"));
            removeCollection.add(new CellDTO(13, 13, "13"));
            removeCollection.add(new CellDTO(13, 14, "14"));
            removeCollection.add(new CellDTO(14, 15, "10"));
            removeCollection.add(new CellDTO(14, 16, "10"));
            removeCollection.add(new CellDTO(15, 15, "10"));
            removeCollection.add(new CellDTO(15, 16, "10"));

            transaction = closeableSession.getSession().beginTransaction();

            for (CellDTO cell : removeCollection){

                closeableSession.getSession().save(cell);

            }

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "deleteCell")
    public Object[][] dataProviderDeleteCellTrue(){
        return new Object[][]{
                {13, 13}
        };
    }


    @Test(groups = "delete", dataProvider = "deleteCell")
    public void testRemoveCellTrue(int rowId, int columnId) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertTrue(hibernateWorker.removeCell(rowId, columnId));

    }


    @DataProvider(name = "deleteCellEmpty")
    public Object[][] dataProviderDeleteCellEmpty(){
        return new Object[][]{
                {13, 14}
        };
    }


    @Test(groups = "delete", dataProvider = "deleteCell")
    public void testRemoveCellEmpty(int rowId, int columnId) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        hibernateWorker.removeCell(rowId, columnId);

        Assert.assertFalse(hibernateWorker.verifyCell(rowId, columnId));

    }


    @DataProvider(name = "deleteRow")
    public Object[][] dataProviderDeleteRow(){
        return new Object[][]{
                {15}
        };
    }


    @Test(groups = "delete", dataProvider = "deleteRow")
    public void testRemoveRow(int rowId) throws SQLException {

        HibernateWorkerDAO hibernateWorkerDAO = new HibernateWorkerDAO();

        hibernateWorkerDAO.removeRow(rowId);

        Assert.assertFalse(hibernateWorkerDAO.verifyRowId(rowId));

    }


    @DataProvider(name = "deleteColumnIterator")
    public Object[][] dataProviderDeleteColumn(){
        return new Object[][]{
                {10}
        };
    }


    @Test(groups = "delete", dataProvider = "deleteColumnIterator")
    public void testRemoveColumn(int columnId) throws SQLException {

        HibernateWorkerDAO hibernateWorkerDAO = new HibernateWorkerDAO();

        hibernateWorkerDAO.removeColumn(columnId);

        List<CellDTO> resultDelete = hibernateWorkerDAO.getColumn(columnId);

        Assert.assertTrue(resultDelete.isEmpty());

    }


    @BeforeGroups(groups = "changeData")
    public void createNewRowsForSet(){

        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            List<CellDTO> setCollection = new LinkedList<>();
            setCollection.add(new CellDTO(21, 20, "10"));
            setCollection.add(new CellDTO(21, 21, "11"));
            setCollection.add(new CellDTO(22, 20, "10"));
            setCollection.add(new CellDTO(22, 21, "11"));
            setCollection.add(new CellDTO(23, 23, "13"));
            setCollection.add(new CellDTO(23, 24, "14"));
            setCollection.add(new CellDTO(24, 25, "10"));
            setCollection.add(new CellDTO(24, 26, "10"));
            setCollection.add(new CellDTO(25, 25, "10"));
            setCollection.add(new CellDTO(25, 26, "10"));

            transaction = closeableSession.getSession().beginTransaction();

            for (CellDTO cell : setCollection){

                closeableSession.getSession().save(cell);

            }

            transaction.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "setCellTrue")
    public Object[][] dataProviderSetCellTrue(){
        return new Object[][]{
                {25, 25, "new25/25"}
        };
    }


    @Test(groups = "changeData", dataProvider = "setCellTrue")
    public void testSetCellNotException(int rowId, int columnId, String newValue) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertTrue(hibernateWorker.setCellValue(rowId, columnId, newValue));

    }


    @DataProvider(name = "setCellEmpty")
    public Object[][] dataProviderSetCellEmpty(){
        return new Object[][]{
                {25, 26, "new25/26", 1}
        };
    }


    @Test(groups = "changeData", dataProvider = "setCellEmpty")
    public void testSetCellEmpty(int rowId, int columnId, String newValue, int amountCell) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        hibernateWorker.setCellValue(rowId, columnId, newValue);

        Assert.assertTrue(hibernateWorker.findValue(newValue).size() == amountCell);

    }


    @DataProvider(name = "setRowTrue")
    public Object[][] dataProviderSetRowTrue(){
        return new Object[][]{
                {23, "new23"}
        };
    }


    @Test(groups = "changeData", dataProvider = "setRowTrue")
    public void testSetRowNotException(int rowId, String newValue) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertTrue(hibernateWorker.setRow(rowId, newValue));

    }


    @DataProvider(name = "setRow")
    public Object[][] dataProviderSetRow(){
        return new Object[][]{
                {24, "new24", 2}
        };
    }


    @Test(groups = "changeData", dataProvider = "setRow")
    public void testSetRow(int rowId, String newValue, int amountResult) throws SQLException {

        HibernateWorkerDAO hibernateWorkerDAO = new HibernateWorkerDAO();

        hibernateWorkerDAO.setRow(rowId, newValue);

        Assert.assertTrue(hibernateWorkerDAO.findValue(newValue).size() == amountResult);

    }


    @DataProvider(name = "setColumnTrue")
    public Object[][] dataProviderSetColumnTrue(){
        return new Object[][]{
                {20, "new20"}
        };
    }


    @Test(groups = "changeData", dataProvider = "setColumnTrue")
    public void testSetColumnNotException(int columnId, String newValue) throws SQLException {

        HibernateWorkerDAO hibernateWorker = new HibernateWorkerDAO();

        Assert.assertTrue(hibernateWorker.setColumn(columnId, newValue));

    }


    @DataProvider(name = "setColumn")
    public Object[][] dataProviderSetColumn(){
        return new Object[][]{
                {21, "new21", 2}
        };
    }


    @Test(groups = "changeData", dataProvider = "setColumn")
    public void testSetColumn(int columnId, String newValue, int amountResult) throws SQLException {

        HibernateWorkerDAO hibernateWorkerDAO = new HibernateWorkerDAO();

        hibernateWorkerDAO.setColumn(columnId, newValue);

        Assert.assertTrue(hibernateWorkerDAO.findValue(newValue).size() == amountResult);

    }
}
