package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dto.Action;
import com.j2core.sts.minisqlexcel.dto.CellDTO;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;
import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Created by sts on 9/12/16.
 */
public class CreatorUserOutputTest {


    @BeforeGroups(groups = "createUserDataDTO")
    public UserDataDTO createUserData (){

        UserDataDTO userData = new UserDataDTO(Action.GET);
        userData.setRowId(1);
        userData.setValue("1:1+1:2");
        return userData;
    }



    @Test(groups = "createUserDataDTO")
    public void testCreateUserOutputGetDataPositive(){

        List<CellDTO> cellDTOList = new LinkedList<>();
        cellDTOList.add(new CellDTO(1, 1, "5"));
        cellDTOList.add(new CellDTO(1, 2, "55"));

        String result = " Date in 1 row is column A = 5; column B = 55;";

        Assert.assertEquals(CreatorUserOutput.createUserOutputGetData(createUserData(), cellDTOList), result);

    }


    @Test(groups = "createUserDataDTO")
    public void testCreateUserOutputGetDataNegative() {

        String result = " Data isn't exist.";

        Assert.assertEquals(CreatorUserOutput.createUserOutputGetData(createUserData(), new LinkedList<CellDTO>()), result);

    }


    @Test(groups = "createUserDataDTO")
    public void testCreateUserOutputRemoveDataPositive(){

        String result = " Date in 1 row  remove successfully.";

        Assert.assertEquals(CreatorUserOutput.createUserOutputRemoveData(createUserData(), true), result);

    }


    @Test(groups = "createUserDataDTO")
    public void testCreateUserOutputRemoveDataNegative(){

        String result = " Date in 1 row  remove unsuccessfully.";

        Assert.assertEquals(CreatorUserOutput.createUserOutputRemoveData(createUserData(), false), result);


    }


    @DataProvider(name = "setData")
    public Object[][] dataProviderVerificationNumber(){
        return new Object[][]{
                {" Change data in A1 is successfully.", true, true},
                {" Data in cell A1 isn't exist. Create new cell is successfully.", true, false},
                {" Data in cell A1 isn't exist. Create new cell is unsuccessfully.", false, false},
                {" Change data in A1 is unsuccessfully.", false, true},

        };
    }


    @Test(groups = "createUserDataDTO", dataProvider = "setData")
    public void testCreateUserOutputSetData(String result, boolean resultAction, boolean flagExist){

        UserDataDTO userData = createUserData();
        userData.setColumnId(1);

        Assert.assertEquals(CreatorUserOutput.createUserOutputSetData(userData, resultAction, flagExist), result);

    }


    @Test(groups = "createUserDataDTO")
    public void testCreateUserOutputCalculateExpression(){

        String result = "  Expression 1:1+1:2 = 555.005.";

        Assert.assertEquals(CreatorUserOutput.createUserOutputCalculateExpression(createUserData(), 555.005), result);

    }


    @DataProvider(name = "notData")
    public Object[][] dataProviderNotData(){
        return new Object[][]{
                {"876", Action.FIND},
                {"43++", Action.CALCULATE},

        };
    }


    @Test(dataProvider = "notData")
    public void testCreateUserOutputNotData(String value, Action action){

        UserDataDTO userDataDTO = new UserDataDTO(action);
        userDataDTO.setValue(value);
        String result = " Data in " + value + " aren't exist.";

        Assert.assertEquals(result, CreatorUserOutput.createUserOutputNotData(userDataDTO));
    }


    @Test
    public void testCreateUserOutputFindValue(){

        List<CellDTO> cellDTOList = new LinkedList<>();
        cellDTOList.add(new CellDTO(1, 1, "5"));
        cellDTOList.add(new CellDTO(25, 23, "5"));

        UserDataDTO userData = new UserDataDTO(Action.FIND);
        userData.setValue("5");

        String result = " The desired value = 5 is in: A1; W25; ";

        Assert.assertEquals(result, CreatorUserOutput.createUserOutputFindValue(userData, cellDTOList));

    }

}
