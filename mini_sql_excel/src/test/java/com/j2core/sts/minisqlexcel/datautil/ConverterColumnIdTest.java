package com.j2core.sts.minisqlexcel.datautil;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by sts on 9/14/16.
 */
public class ConverterColumnIdTest {

    @DataProvider(name = "convertColumnName")
    public Object[][] dataProviderFindValue() {
        return new Object[][]{
                {"AA", 27},
                {"A", 1},
                {"AB", 28},
                {"AZ", 52},
                {"Z", 26},
                {"BCD", 1434},
                {"ABCD", 19010},
        };
    }


    @Test(dataProvider = "convertColumnName")
    public void testConvertColumnName(String columnName, int columnId){

        Assert.assertEquals(columnId, ConverterColumnId.convertColumnName(columnName));

    }




    @Test(dataProvider = "convertColumnName")
    public void testConvertColumnId(String columnName, int columnId){

        Assert.assertEquals(columnName, ConverterColumnId.convertColumnId(columnId));

    }
}
