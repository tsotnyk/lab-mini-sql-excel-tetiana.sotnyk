package com.j2core.sts.minisqlexcel.util;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by sts on 9/29/16.
 */
public class DBSessionProviderTest {

    @Test
    public void testGetProviderInstance(){

        Assert.assertNotNull(DBSessionProvider.getProviderInstance());

    }


    @Test
    public void testGetSessionFactory() throws Exception {

        Assert.assertNotNull(DBSessionProvider.getProviderInstance().getSessionFactory());

    }

}
