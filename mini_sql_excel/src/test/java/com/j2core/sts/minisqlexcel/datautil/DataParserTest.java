package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.dao.WorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.ParsedExpressionDTO;
import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import org.testng.Assert;
import org.testng.annotations.*;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class DataParserTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db1";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT(11) NOT NULL," +
            "                                                           columnId INT(11) NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);


    @BeforeClass
    public void createTestDB() throws IOException {

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement()){

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry unsuccessful create DB!");
        }
    }


    @DataProvider(name = "number")
    public Object[][] dataProviderVerificationNumber(){
        return new Object[][]{
                {"0", 0.0},
                {"2.83746834765", 2.83746834765},
                {"234652764", 234652764},
        };
    }


    @Test(dataProvider = "number")
    public void testParseNumber(String number, double result) throws ParseProcessException {

        DataParser parser = new DataParser();

        Assert.assertEquals(parser.parseNumber(number), result);

    }


    @DataProvider(name = "numberException")
    public Object[][] dataProviderVerificationNumberException(){
        return new Object[][]{
                {"khufduy"},
                {"2c"},
                {"c34"},
                {"3.57.56567"}
        };
    }


    @Test(dataProvider = "numberException", expectedExceptions = ParseProcessException.class)
    public void testParseNumberException(String number) throws ParseProcessException {

        DataParser parser = new DataParser();

        parser.parseNumber(number);

    }


    @DataProvider(name = "expression")
    public Object[][] dataProviderVerificationExpression(){
        return new Object[][]{
                {"caa+cbb-cc:c", 3, 2},
                {"l1+l2-l3", 3, 2},
                {"3434:3434+3:5/4*2323", 4, 3}
        };
    }


    @Test(dataProvider = "expression")
    public void testParsingExpression(String expression, int amountNumbers, int amountActions) throws ParseProcessException, IOException {

        DataParser parser = new DataParser();

        ParsedExpressionDTO parsedExpression = parser.parseExpression(expression);

        Assert.assertTrue(parsedExpression.getCells().size() == amountNumbers && parsedExpression.getActions().size() == amountActions );

    }


    @Test
    public void testChangeValueType() throws ParseProcessException {

        DataParser parser = new DataParser();
        List<String> originalList = new LinkedList<>();
        originalList.add("3.27635");
        originalList.add("0");
        originalList.add("26354");

        List<Double> newList = parser.changeValueType(originalList);

        Assert.assertEquals(newList.size(), originalList.size());

    }


    @BeforeGroups(groups = "substitution")
    public void createTestDataForSubstitution(){

        try (Connection connection = connectionPool.getConnection(); Statement statement = connection.createStatement()){

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 1, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 2, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 1, '55')");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    @Test(groups = "substitution")
    public void testSubstitutionData() throws IOException, ParseProcessException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());
        DataParser parser = new DataParser();
        List<Character> actions = null;
        List<String> value = new LinkedList<>();
        value.add("1:1");
        value.add("1:2");
        value.add("2:1");
        ParsedExpressionDTO testData = new ParsedExpressionDTO(actions, value);

        ParsedExpressionDTO parsedExpressionDTO = parser.substitutionData(testData, worker);

        Assert.assertEquals(parsedExpressionDTO.getCells().size(), value.size());

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}
