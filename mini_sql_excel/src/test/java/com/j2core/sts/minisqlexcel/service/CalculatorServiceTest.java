package com.j2core.sts.minisqlexcel.service;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.dao.WorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.DataParser;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import org.testng.Assert;
import org.testng.annotations.*;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.*;


/**
 * Created by sts on 9/9/16.
 */
public class CalculatorServiceTest {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db4";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT(11) NOT NULL," +
            "                                                           columnId INT(11) NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);


    @BeforeClass
    public void createTestDB() throws IOException {

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement()){

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 1, '5')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 2, '5')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 1, '55')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(4, 1, '1')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(3, 2, '2')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 2, '3')");

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry unsuccessful create DB!");
        }
    }


    @DataProvider(name = "calculateExpression")
    public Object[][] dataProviderCalculateExpression(){
        return new Object[][]{
                {"A1*(B1-A2)+50", -200},
        };
    }


    @Test(dataProvider = "calculateExpression")
    public void testCalculateExpression(String expression, double result) throws IOException, ParseProcessException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());
        DataParser dataParser = new DataParser();
        CalculatorService calculatorService = new CalculatorService(worker, dataParser);

        Assert.assertEquals(calculatorService.calculateExpression(expression), result);

    }



    @AfterClass
    public void deleteTestDB() throws SQLException {

        try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
