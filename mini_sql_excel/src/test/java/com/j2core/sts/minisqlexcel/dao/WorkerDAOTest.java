package com.j2core.sts.minisqlexcel.dao;

import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import org.testng.Assert;
import org.testng.annotations.*;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by sts on 8/23/16.
 */
public class WorkerDAOTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT(11) NOT NULL," +
            "                                                           columnId INT(11) NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);


    @BeforeClass
    public void createTestDB() throws IOException {

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement()){

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry unsuccessful create DB!");
        }
    }


    @BeforeGroups(groups = "verifyTake")
    public void createVerifyData(){

        try (Connection connection = connectionPool.getConnection(); Statement statement = connection.createStatement()){

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 1, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 2, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 1, '55')");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "verifyRowId")
    public Object[][] dataProviderVerifyRow(){
        return new Object[][]{
                {1, true},
                {2, true},
                {1, true},
                {5, false}
        };
    }


    @Test(groups = "verifyTake", dataProvider = "verifyRowId")
    public void testVerificationRowId(int rowId, boolean result) throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(worker.verifyRowId(rowId), result);
    }


    @DataProvider(name = "verifyCell")
    public Object[][] dataProviderVerifyColumn(){
        return new Object[][]{
                {1, 1, true},
                {2, 1, true},
                {1, 3, false},
                {5, 5, false}
        };
    }


    @Test(groups = "verifyTake", dataProvider = "verifyCell")
    public void testVerificationColumn(int rowId, int columnId, boolean result) throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(worker.verifyCell(rowId, columnId), result);

    }


    @DataProvider(name = "get.row")
    public Object[][] dataProviderGetRow(){
        return new Object[][]{
                {1, 2},
                {2, 1},
        };
    }


    @Test(groups = "verifyTake", dataProvider = "get.row")
    public void testGetRowPositive(int rowId, int amountCell) throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(amountCell, worker.getRow(rowId).size());

    }


    @Test(groups = "verifyTake")
    public void testGetRowNegative() throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.getRow(12).isEmpty());

    }


    @DataProvider(name = "takeCell")
    public Object[][] dataProviderGetCell(){
        return new Object[][]{
                {1, 1, "5"},
                {2, 1, "55"},
        };
    }


    @Test(groups = "verifyTake", dataProvider = "takeCell")
    public void testGetCellValuePositive(int rowId, int columnId, String result) throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(result, worker.getCellValue(rowId, columnId).get(0).getValue());

    }


    @Test(groups = "verifyTake")
    public void testGetCellValueNegative() throws SQLException, IOException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.getCellValue(12, 1).isEmpty());

    }


    @DataProvider(name = "get.column")
    public Object[][] dataProviderGetColumn(){
        return new Object[][]{
                { 1, 2},
                { 2, 1},
                {4, 0}
        };
    }


    @Test(groups = "verifyTake", dataProvider = "get.column")
    public void testGetColumn(int columnId, int result) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(result, worker.getColumn(columnId).size());

    }


    @DataProvider(name = "create.column")
    public Object[][] dataProviderCreateColumn(){
        return new Object[][]{
                {3, 3, "3"},
        };
    }


    @Test(groups = "verifyTake", dataProvider = "create.column")
    public void testCreateColumnPositive(int rowId, int columnName, String columnValue) throws SQLException, IOException {

        WorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.createCell(rowId, columnName, columnValue));

    }


    @Test(groups = "verifyTake")
    public void testCreateColumnNegative() throws SQLException, IOException {

        WorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.createCell(1, 1, ""));

    }


    @BeforeGroups(groups = "remove")
    public void createRemoveData(){

        try (Connection connection = connectionPool.getConnection(); Statement statement = connection.createStatement()){

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(10, 10, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(10, 5, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(20, 5, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(20, 16, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(20, 3, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(30, 10, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(30, 5, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(40, 10, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(50, 10, '55')");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "removeRow")
    public Object[][] dataProviderDeleteRow(){
        return new Object[][]{
                {10},
        };
    }


    @Test(groups = "remove", dataProvider = "removeRow")
    public void testRemoveRowPositive(int rowId) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.removeRow(rowId));

    }


    @Test(groups = "remove")
    public void testRemoveRowNegative() throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.removeRow(15));

    }


    @DataProvider(name = "removeCell")
    public Object[][] dataProviderDeleteColumn(){
        return new Object[][]{
                {5}
        };
    }


    @Test(groups = "remove", dataProvider = "removeCell")
    public void testRemoveColumnPositive(int columnId) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.removeColumn(columnId));

    }


    @Test(groups = "remove")
    public void testRemoveColumnNegative() throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.removeColumn(15));

    }


    @Test(groups = "remove")
    public void testRemoveCellPositive() throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.removeCell(50, 10));

    }


    @Test(groups = "remove")
    public void testRemoveCellNegative() throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.removeCell(50, 50));

    }


    @BeforeGroups(groups = "set")
    public void createSetData(){

        try (Connection connection = connectionPool.getConnection(); Statement statement = connection.createStatement()){

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(100, 10, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(100, 50, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(200, 50, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(200, 100, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(200, 30, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(300, 100, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(300, 50, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(400, 100, '55')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(500, 100, '55')");


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "set.column")
    public Object[][] dataProviderChangeValue(){
        return new Object[][]{
                {100, "999"}
        };
    }


    @Test(groups = "set", dataProvider = "set.column")
    public void testSetColumnPositive(int columnId, String newValue) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.setColumn(columnId, newValue));

    }


    @Test(groups = "set")
    public void testSetColumnNegative() throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.setColumn(150, "22"));

    }


    @DataProvider(name = "setCellTrue")
    public Object[][] dataProviderSetCell(){
        return new Object[][]{
                {200, 30, "-100"},
        };
    }


    @Test(groups = "set", dataProvider = "setCellTrue")
    public void testSetCellValuePositive(int rowId, int columnId, String newValue) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.setCellValue(rowId, columnId, newValue));

    }


    @DataProvider(name = "setCellFalse")
    public Object[][] dataProviderSetCellFalse(){
        return new Object[][]{
                {200, 1, "-100"},
                {201, 2, "-100"}
        };
    }


    @Test(groups = "set", dataProvider = "setCellFalse")
    public void testSetCellValueNegative(int rowId, int columnId, String newValue) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.setCellValue(rowId, columnId, newValue));

    }


    @DataProvider(name = "setRowTrue")
    public Object[][] dataProviderSetRow(){
        return new Object[][]{
                {100, "-100"},
        };
    }


    @Test(groups = "set", dataProvider = "setRowTrue")
    public void testSetRowPositive(int rowId, String newValue) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertTrue(worker.setRow(rowId, newValue));

    }


    @DataProvider(name = "setRowFalse")
    public Object[][] dataProviderSetRowFalse(){
        return new Object[][]{
                {101, "-100"},
        };
    }


    @Test(groups = "set", dataProvider = "setRowFalse")
    public void testSetRowNegative(int rowId, String newValue) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertFalse(worker.setRow(rowId, newValue));

    }


    @BeforeGroups(groups = "find")
    public void createFindData(){

        try (Connection connection = connectionPool.getConnection(); Statement statement = connection.createStatement()){

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(30, 30, '717')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(30, 31, '717')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(31, 31, '171')");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @DataProvider(name = "find.value")
    public Object[][] dataProviderFindValue(){
        return new Object[][]{
                {"717", 2},
                {"171", 1},
                {"543", 0}
        };
    }


    @Test(groups = "find", dataProvider = "find.value")
    public void testFindValue(String value, int amountCell) throws IOException, SQLException {

        DBWorkerDAO worker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());

        Assert.assertEquals(worker.findValue(value).size(), amountCell);

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
