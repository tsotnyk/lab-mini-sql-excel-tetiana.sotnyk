package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dao.WorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.exception.VerifyProcessException;
import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import org.testng.Assert;
import org.testng.annotations.*;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 8/8/16.
 */
public class VerifierDataTest {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db3";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT(11) NOT NULL," +
            "                                                           columnId INT(11) NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);


    @BeforeClass
    public void createTestDB() throws IOException {

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement()){

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry unsuccessful create DB!");
        }
    }


    @DataProvider(name = "number")
    public Object[][] dataProviderVerificationNumber(){
        return new Object[][]{
                {"0", true},
                {"2.83746834765", true},
                {"234652764576576",true},
                {"khufduy", false},
                {"2c", false},
                {"c34", false}
        };
    }


    @Test(dataProvider = "number")
    public void testVerificationNumber(String number, boolean result) throws VerifyProcessException, IOException {

        DataParser parser = new DataParser();
        VerifierData verifierData = new VerifierData(parser, new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties()));

        Assert.assertEquals(verifierData.verifyNumber(number), result);

    }


    @DataProvider(name = "expressionTrue")
    public Object[][] dataProviderVerificationExpressionTrue(){
        return new Object[][]{
                {"A2+D34*(DR3-FF5)"},
                {"(C8-D5)+A4"}
        };
    }


    @Test(dataProvider = "expressionTrue")
    public void testVerificationExpression(String expression) throws VerifyProcessException, IOException {

        DataParser parser = new DataParser();
        VerifierData verifierData = new VerifierData(parser,  new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties()));

        Assert.assertTrue(verifierData.verificationExpression(expression));

    }


    @DataProvider(name = "expressionException")
    public Object[][] dataProviderVerificationExpressionException(){
        return new Object[][]{
                {"1:3*(3,56+2:3)"}
        };
    }


    @Test(dataProvider = "expressionException")
    public void testVerificationExpressionNegative(String expression) throws VerifyProcessException, IOException {

        DataParser parser = new DataParser();
        VerifierData verifierData = new VerifierData(parser,  new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties()));

        Assert.assertFalse(verifierData.verificationExpression(expression));

    }


    @DataProvider(name = "bracketsValid")
    public Object[][] dataProviderIsBracketValid(){
        return new Object[][]{
                {"1:3*(3,56+2:3)", true},
                {"(5-34))*576", false},
                {"((3-5/3)+3", false},
                {"(2-6))+(23", false}
        };
    }


    @Test(dataProvider = "bracketsValid")
    public void testIsBracketsValid(String expression, boolean result) throws IOException {

        DataParser parser = new DataParser();
        VerifierData verifierData = new VerifierData(parser,  new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties()));

        Assert.assertEquals(verifierData.isBracketsValid(expression), result);

    }


    @AfterClass
    public void deleteTestDB() throws SQLException {

        try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}




