package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dao.WorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.Action;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;
import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import snaq.db.ConnectionPool;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by sts on 8/23/16.
 */
public class UserInputParserTest {


    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "test_db2";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT(11) NOT NULL," +
            "                                                           columnId INT(11) NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);


    @BeforeClass
    public void createTestDB() throws IOException {

        try(Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement()){

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(0, 1, 'name')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(0, 5, 'cash')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 0, 'Den')");
            testStatement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 5, '23')");

        } catch(Exception e){
            e.printStackTrace();
            System.out.println("Sorry unsuccessful create DB!");
        }
    }

    @Test
    public void testParseCellData() throws ParseProcessException, IOException, SQLException {

        UserInputParser parser = new UserInputParser();
        UserDataDTO testData = new UserDataDTO(Action.GET);
        testData.setRowId(1);
        testData.setColumnId(2);

        UserDataDTO userData = new UserDataDTO(Action.GET);
        userData = parser.parseCellData("B1", userData);


        Assert.assertEquals(userData, testData);

    }


    @Test
    public void testParseUserInput() throws ParseProcessException, IOException, SQLException {

        UserInputParser parser = new UserInputParser();

        UserDataDTO testData = new UserDataDTO(Action.SET);
        testData.setRowId(1);
        testData.setColumnId(2);
        testData.setValue("3");

        String userQuery = "set B1 3";

        Assert.assertEquals(parser.parseUserInput(userQuery), testData);

    }



    @AfterClass
    public void deleteTestDB() throws SQLException {

        try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()){

            String sql = "DROP DATABASE " + NAME_DB;
            stmt.executeUpdate(sql);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
