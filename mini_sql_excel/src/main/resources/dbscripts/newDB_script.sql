CREATE TABLE cells (
                             rowId INT(11) NOT NULL,
                             columnId INT(11) NOT NULL,
                             value TEXT,
                             PRIMARY KEY (rowId, columnId)
                            );