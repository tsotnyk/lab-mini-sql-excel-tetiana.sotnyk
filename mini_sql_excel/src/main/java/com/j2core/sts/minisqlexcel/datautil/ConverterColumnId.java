package com.j2core.sts.minisqlexcel.datautil;

/**
 * Created by sts on 9/14/16.
 */

/**
 * The class convert column name to the id and id to the name
 */
public class ConverterColumnId {

    /**
     * The method convert column name in to column Id
     *
     * @param columnName     column Name
     * @return               column Id
     */
    public static int convertColumnName(String columnName){

        int columnId = 0;
        int amountLatter = columnName.length();
        for (int i = 0; i < amountLatter - 1; i++){

            columnId = (int) (columnId + ((columnName.charAt(i) - 64) * (Math.pow(26.0, amountLatter - (1+i)))));

        }
        columnId = columnId + (columnName.charAt(columnName.length()-1) - 64);

        return columnId;

    }


    /**
     * The method convert column id in to column name
     *
     * @param columnId       column id
     * @return               column name
     */
    public static String convertColumnId(int columnId) {

        int amountColumn = columnId;
        int amountLatter;

        int indexLastLatter = columnId % 26;
        if (indexLastLatter == 0){
            indexLastLatter = 26;
        }
        char lastLatter = (char) (indexLastLatter + 64);
        amountLatter = 1;

        while (amountColumn > (Math.pow(26.0, amountLatter))) {

            amountLatter++;

        }

        char[] arrayColumnName = new char[amountLatter];
        arrayColumnName[amountLatter-1] = lastLatter;

        amountColumn = amountColumn - indexLastLatter;
        amountColumn = amountColumn / 26;
        int[] indexLatterArray = new int[arrayColumnName.length -1];

        while (amountColumn > 0) {

            indexLatterArray[indexLatterArray.length-1] = indexLatterArray[indexLatterArray.length-1] + 1;
            amountColumn = amountColumn - 1;

            for (int i = indexLatterArray.length; i > 0 ; i--){

                if (indexLatterArray[i-1] > 26){

                    indexLatterArray[i - 2] = indexLatterArray [i - 2] + 1;
                    indexLatterArray[i-1] = 1;

                }
            }
        }

        for (int j = 0; j < arrayColumnName.length - 1; j++){

            arrayColumnName[j] = (char) ((char) indexLatterArray[j] + 64);

        }


        StringBuilder columnName = new StringBuilder();

        for (char anArrayColumnName : arrayColumnName) {

            columnName.append(anArrayColumnName);

        }

        return columnName.toString();
    }
}
