package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dto.Action;

/**
 * Created by sts on 8/25/16.
 */

/**
 * The class verify user's action
 */
public class VerifierUserAction {

    /**
     * The method verify user's action
     *
     * @param userQuery     user's query
     * @return              valid or not user's action
     */
    public static boolean verifyAction(String userQuery){

        int index = userQuery.indexOf(" ");
        if (index > -1) {
            for (Action action : Action.values()) {

                if (action.getAction().equals(userQuery.substring(0, index))){
                    return true;
                }
            }
        }
        return false;
       }


    /**
     * The method verify this user's action is "exit" or not
     *
     * @param userQuery     user's query
     * @return   user's action is "exit or not"
     */
    public static boolean verifyExitAction(String userQuery){

        int index = userQuery.indexOf(" ");
        return Action.EXIT.getAction().equals(userQuery.substring(0, index));

    }
}
