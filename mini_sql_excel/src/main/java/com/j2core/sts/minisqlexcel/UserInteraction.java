package com.j2core.sts.minisqlexcel;

import com.j2core.sts.minisqlexcel.datautil.UserInputParser;
import com.j2core.sts.minisqlexcel.datautil.VerifierUserAction;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;
import com.j2core.sts.minisqlexcel.service.PerformerService;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created by sts on 8/25/16.
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "DM_DEFAULT_ENCODING")
public class UserInteraction {

    private static final Logger LOGGER = Logger.getLogger(UserInteraction.class); // object for save log information
    private static final Scanner SCANNER = new Scanner(System.in);                // object for scanning user's information
    private PerformerService performerService;
    private UserInputParser parser;


    /**
     * Constructor
     *
     * @param performerService    performer service
     * @param parser              parser user's input
     */
    public UserInteraction(PerformerService performerService, UserInputParser parser) {
        this.performerService = performerService;
        this.parser = parser;
    }

    /**
     * The method do user's interaction
     *
     * @throws IOException   if has problems with properties files
     * @throws SQLException  if some thing wrong with DB
     */
    public void doUserInteraction() throws IOException, SQLException {
        UserDataDTO userData;

        String userSelect;

        System.out.print(" Good day! What do you want do?");

        while (true) {
            userSelect = SCANNER.nextLine();

            if (VerifierUserAction.verifyAction(userSelect)) {

                if (VerifierUserAction.verifyExitAction(userSelect)) {

                    break;

                } else {

                    try {
                        userData = parser.parseUserInput(userSelect);
                    } catch (ParseProcessException e) {
                        LOGGER.error(e);
                        break;
                    }

                    System.out.println(performerService.processUserInput(userData));
                    System.out.println(" What do you want do?");
                }
            } else System.out.println("Some thing wrong! Please try again.");
        }
        System.out.println("Good day! Bye!");
    }
}
