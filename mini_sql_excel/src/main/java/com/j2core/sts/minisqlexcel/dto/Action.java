package com.j2core.sts.minisqlexcel.dto;

/**
 * Created by sts on 8/22/16.
 */

/**
 * Enum with user's actions
 */
public enum Action {

    EXIT("exit"),
    HELP("help"),
    SET("set"),
    GET("get"),
    CALCULATE("calculate"),
    REMOVE("remove"),
    FIND("find");


    private final String action;     // user's action


    Action(String action){
        this.action = action;
    }

    public String getAction(){
        return action;
    }
}
