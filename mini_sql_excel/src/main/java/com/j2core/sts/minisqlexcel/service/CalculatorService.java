package com.j2core.sts.minisqlexcel.service;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.DataParser;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.ParsedExpressionDTO;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by sts on 8/15/16.
 */

/**
 * Class calculate user's expression
 */
public class CalculatorService {

    private DBWorkerDAO worker;
    private DataParser parser;// object for parse user's expression


    /**
     * Constructor
     *
     * @param worker        object for work with DB
     * @param dataParser    object for parse user's data
     */
    public CalculatorService(DBWorkerDAO worker, DataParser dataParser){
        this.worker = worker;
        this.parser = dataParser;
    }


    /**
     * The method calculate user's expression
     *
     * @param expression                  user's expression
     * @return                            result calculate user's expression
     * @throws ParseProcessException      if expression isn't valid
     * @throws SQLException               if some thing wrong with DB
     */
    public Double calculateExpression(String expression) throws ParseProcessException, SQLException {

        String calculateExpression = expression;
        List<Double> numbers;

        String value;
        int indexOpenBracket;
        int indexCloseBracket;

        indexCloseBracket = calculateExpression.indexOf(')');
        while (indexCloseBracket > -1){
            String tmpExpression = calculateExpression;
            indexOpenBracket = calculateExpression.substring(0, indexCloseBracket).lastIndexOf('(');
            value = calculateExpression(calculateExpression.substring(indexOpenBracket+1, indexCloseBracket)).toString();
            if (indexOpenBracket > 0){
                calculateExpression = tmpExpression.substring(0, indexOpenBracket);
            }
            calculateExpression = calculateExpression + value;
            if (indexCloseBracket < tmpExpression.length()-2){
                calculateExpression = calculateExpression + tmpExpression.substring(indexCloseBracket+1);
            }

            indexCloseBracket = calculateExpression.indexOf(')');
        }

        ParsedExpressionDTO parsedExpressionDTO = parser.parseExpression(calculateExpression);
        parsedExpressionDTO = parser.substitutionData(parsedExpressionDTO, worker);
        numbers = parser.changeValueType(parsedExpressionDTO.getCells());

        return calculateMathExpression(parsedExpressionDTO.getActions(), numbers);
    }


    /**
     * The method calculation data
     *
     * @param action      action for calculate
     * @param numbers     value for calculate
     * @return   result calculation
     * @throws ArithmeticException  if check division by zero
     */
    public  Double calculateMathExpression(List<Character> action, List<Double> numbers) throws ArithmeticException{

        int index_availability;
        Double data1, data2, sum;
        final double INACCURACY = 0.000001;

        // Performance all processUserInput of '/'
        index_availability = action.lastIndexOf('/');

        while (index_availability > -1) {
            data1 = numbers.get(index_availability);
            data2 = numbers.get(index_availability + 1);
            //  Check division by zero
            if (data2 > 0.0 && data2 <= INACCURACY) {
                throw new ArithmeticException(" Sorry! Division by zero is inadmissible.");
            } else {
                numbers.remove(index_availability + 1);
                sum = data1 / data2;
                numbers.set(index_availability, sum);
                action.remove(index_availability);
            }

            index_availability = action.lastIndexOf('/');
        }

        // Performance all processUserInput of '*'
        index_availability = action.lastIndexOf('*');

        while (index_availability > -1) {
            data1 = numbers.get(index_availability);
            data2 = numbers.get(index_availability + 1);
            numbers.remove(index_availability + 1);
            sum = data1 * data2;
            numbers.set(index_availability, sum);
            action.remove(index_availability);

            index_availability = action.lastIndexOf('*');
        }

        // Performance all processUserInput of '+' and '-'
        while (action.size() > 0) {
            data1 = numbers.get(0);
            data2 = numbers.get(1);
            numbers.remove(1);
            if ('+' == action.get(0)) {
                sum = data1 + data2;
            } else {
                sum = data1 - data2;
            }
            numbers.set(0, sum);
            action.remove(0);
        }

        return numbers.get(0);
    }

}
