package com.j2core.sts.minisqlexcel.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by sts on 9/1/16.
 */
@edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = {"EQ_DOESNT_OVERRIDE_EQUALS","SE_NO_SERIALVERSIONID"}, justification = "Exception detail hide")
public class PropertyLoaderLazyLoadSingleton extends Properties{

    private static volatile PropertyLoaderLazyLoadSingleton instance = null;

    private Properties properties;
    private static final String MESSAGE_PROPERTIES_NAME = "message.properties";
    private static final String SQL_QUERY_PROPERTIES_NAME = "sqlquery.properties";


    public Properties getProperties() {
        return this.properties;
    }

    private PropertyLoaderLazyLoadSingleton() throws IOException {

        properties = new Properties();
        InputStream inputStreamMessageProperties = getClass().getClassLoader().getResourceAsStream(MESSAGE_PROPERTIES_NAME);
        InputStream inputStreamSQLQueryProperties = getClass().getClassLoader().getResourceAsStream(SQL_QUERY_PROPERTIES_NAME);

        if (inputStreamMessageProperties != null && inputStreamSQLQueryProperties != null) {
            properties.load(inputStreamMessageProperties);
            properties.load(inputStreamSQLQueryProperties);
        }

    }

    public static PropertyLoaderLazyLoadSingleton getInstance() throws IOException {

        if (instance == null){
            synchronized (PropertyLoaderLazyLoadSingleton.class) {
                if (instance ==null) {
                    instance = new PropertyLoaderLazyLoadSingleton();
                }
            }
        }
        return instance;
    }

}
