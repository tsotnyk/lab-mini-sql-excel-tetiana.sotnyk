package com.j2core.sts.minisqlexcel.dto;

/**
 * Created by sts on 9/7/16.
 */

import javax.persistence.*;
import java.io.Serializable;

/**
 * Account model.
 */
@Entity
@IdClass(CellId.class)
@Table(name = "Cells")
public class CellDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "rowId")
    private int rowId;

    @Id
    @Column(name = "columnId")
    private int columnId;

    @Column(name = "value")
    private String value;


    /**
     * Constructor
     */
    public CellDTO() {
    }


    public CellDTO(CellId cellId, String value){

        this.rowId = cellId.getRowId();
        this.columnId = cellId.getColumnId();
        this.value = value;

    }

    /**
     * Constructor
     *
     *  @param rowId         cell's row's id
     * @param columnId      cell's column id
     * @param value         cell's value
     */
    public CellDTO(int rowId, int columnId, String value) {
        this.rowId = rowId;
        this.columnId = columnId;
        this.value = value;
    }

    public int getRowId() {
        return rowId;
    }

    public int getColumnId() {
        return columnId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellDTO cellDTO = (CellDTO) o;

        if (rowId != cellDTO.rowId) return false;
        if (columnId != cellDTO.columnId) return false;
        return value.equals(cellDTO.value);

    }

    @Override
    public int hashCode() {
        int result = rowId;
        result = 31 * result + columnId;
        return result;
    }

    @Override
    public String toString() {
        return "CellDTO{" +
                "rowId=" + rowId +
                ", columnId=" + columnId +
                ", value='" + value + '\'' +
                '}';
    }
}
