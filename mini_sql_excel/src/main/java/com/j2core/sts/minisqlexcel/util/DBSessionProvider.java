package com.j2core.sts.minisqlexcel.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by sts on 9/29/16.
 */
public class DBSessionProvider {

    private final static Logger LOGGER = LoggerFactory.getLogger(DBSessionProvider.class);
    private static volatile DBSessionProvider providerInstance;
    private SessionFactory sessionFactory;


    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    private DBSessionProvider() {

        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        }catch (Throwable e){
            LOGGER.error( "Initial SessionFactory creation failed." + e.fillInStackTrace());
        }

    }

    public static DBSessionProvider getProviderInstance(){

        if (providerInstance == null){
            synchronized (DBSessionProvider.class) {
                if (providerInstance ==null) {
                    providerInstance = new DBSessionProvider();
                }
            }
        }
        return providerInstance;
    }
}
