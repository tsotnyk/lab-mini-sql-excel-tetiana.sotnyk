package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.datautil.exception.VerifyProcessException;
import org.apache.log4j.Logger;

/**
 * Created by sts on 8/7/16.
 */

/**
 * This class validate values data
 */
public class VerifierData {

    private static final String VALID_SYMBOLS = "-+*/()1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final Logger LOGGER = Logger.getLogger(VerifierData.class);
    DataParser parser;
    DBWorkerDAO worker;

    /**
     * Constructor VerifierData's class
     *
     * @param parser  class for parsing data
     */
    public VerifierData(DataParser parser, DBWorkerDAO worker) {
        this.parser = parser;
        this.worker = worker;
    }


    /**
     * The method verification number's value
     *
     * @param value  number's value
     * @return valid number's value
     * @throws VerifyProcessException  if number's value isn't number
     */
    public boolean verifyNumber(String value) throws VerifyProcessException {

        try {
            parser.parseNumber(value);
        } catch (ParseProcessException parsExpression) {
            LOGGER.error(parsExpression);
            return false;
        }

        return true;
    }


    /**
     * The method verification expression.
     *
     * @param expression  expression for verification
     * @return  expression valid or no
     */
    public boolean verificationExpression(String expression){

        for (int i = 0; i < expression.length(); i++){

            if (VALID_SYMBOLS.indexOf(expression.charAt(i)) < 0){
                LOGGER.info("This expression has not valid data - " + expression.charAt(i));
                return false;
            }
        }
        return true;
    }


    /**
     * The method is verification check correctness of the brackets in expression
     *
     * @param expression Expression for verification
     * @return  is brackets valid in expression
     */
    public boolean isBracketsValid (String expression){

        int bracketBalance = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                bracketBalance++;
            }
            else if (expression.charAt(i) == ')') {
                bracketBalance--;
            }
            if(bracketBalance<0) return false;
        }
        return bracketBalance == 0;
    }


    /**
     * The method trim space in original expression
     *
     * @param expression    original expression
     * @return              changed expression
     */
    public String trimSpace(String expression){

        StringBuilder newExpression = new StringBuilder(expression);

        int index = newExpression.lastIndexOf(" ");
        while (index > -1){

            newExpression.deleteCharAt(index);
            index = newExpression.lastIndexOf(" ");
        }

        return newExpression.toString();
    }


    /**
     * The method verify this expression. This expression is regular or calculate row(column)
     *
     * @param expression   user's expression
     * @return             if calculate row or column - true, else false
     */
    public boolean verifyRowOrColumnCalculation(String expression){

        int index = 0;
        while (index < expression.length()){
            if (expression.charAt(index) == '-' || expression.charAt(index) == '*' ||
                    expression.charAt(index) == '/'){
                return false;
            }
            if (expression.charAt(index) == '+' && expression.charAt(index+1) == '+' && index+1 == expression.length()-1){
                    return true;
            }
            index++;
        }

        return false;
    }

}






