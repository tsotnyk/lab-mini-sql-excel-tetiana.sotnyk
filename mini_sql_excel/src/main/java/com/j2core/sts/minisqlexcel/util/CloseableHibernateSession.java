package com.j2core.sts.minisqlexcel.util;

import org.hibernate.Session;

/**
 * Created by sts on 10/4/16.
 */
public class CloseableHibernateSession implements AutoCloseable {

    private final Session session;

    public CloseableHibernateSession(Session session) {
        this.session = session;
    }

    @Override
    public void close() throws Exception {

        if (session != null) {
            session.close();
        }
    }

    public Session getSession(){
        return session;
    }
}
