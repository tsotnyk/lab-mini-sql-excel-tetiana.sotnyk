package com.j2core.sts.minisqlexcel.dao;

import com.j2core.sts.minisqlexcel.dto.CellDTO;
import snaq.db.ConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * Created by sts on 8/21/16.
 */

/**
 * Class work with Data Base. Create data in DB, take data from DB, change data in DB, and delete data in DB
 */
public class WorkerDAO implements DBWorkerDAO {

    private Properties properties;             // properties with SQL query
    private ConnectionPool connectionPool;    // pool with connection to the DB

    /**
     * Constructor WorkerDAO object
     *
     * @param connectionPool properties with SQL query
     * @param properties     pool with connection to the DB
     */
    public WorkerDAO(ConnectionPool connectionPool, Properties properties) {
        this.properties = properties;
        this.connectionPool = connectionPool;
    }


    /**
     * The method verification row's id
     *
     * @param rowId row's name
     * @return exist or not exist this row's id in DB
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean verifyRowId(int rowId) throws SQLException {

        boolean result = false;
        String selectValidRow = properties.getProperty("check.row");

        try (Connection connection = connectionPool.getConnection(); PreparedStatement psValidRow = connection.prepareStatement(selectValidRow)) {

            psValidRow.setInt(1, rowId);

            try (ResultSet resultSet = psValidRow.executeQuery()) {

                if (resultSet.next()) {
                    result = true;
                }
            }
        }
        return result;
    }


    /**
     * The method verification column's name
     *
     * @param rowId    row's id
     * @param columnId column's name
     * @return exist or not exist this column's name for this row in DB
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean verifyCell(int rowId, int columnId) throws SQLException {

        boolean result = false;
        String selectValidColumn = properties.getProperty("check.column");

        try (Connection connection = connectionPool.getConnection(); PreparedStatement psValidColumn = connection.prepareStatement(selectValidColumn)) {

            psValidColumn.setInt(1, rowId);
            psValidColumn.setInt(2, columnId);

            try (ResultSet resultSet = psValidColumn.executeQuery()) {
                if (resultSet.next()) {
                    result = true;
                }
            }
        }
        return result;
    }


    /**
     * The method take information about row
     *
     * @param rowId row's id
     * @return information about row
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public List<CellDTO> getRow(int rowId) throws SQLException {

        List<CellDTO> result = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (!verifyRowId(rowId)) {
                return result;
            }

            String selectRow = properties.getProperty("get.row");

            try (PreparedStatement psSelectRow = connection.prepareStatement(selectRow)) {

                psSelectRow.setInt(1, rowId);

                connection.setAutoCommit(false);

                try (ResultSet resultSet = psSelectRow.executeQuery()) {

                    while (resultSet.next()) {

                        result.add(new CellDTO(rowId, resultSet.getInt("columnId"), resultSet.getString("value")));

                    }
                }
                connection.commit();
            }
        }
        return result;
    }


    /**
     * The method take information about column
     *
     * @param columnId column's id
     * @return information about column
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public List<CellDTO> getColumn(int columnId) throws SQLException {

        List<CellDTO> result = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection()) {

            String selectRow = properties.getProperty("get.column");

            try (PreparedStatement psSelectColumn = connection.prepareStatement(selectRow)) {

                psSelectColumn.setInt(1, columnId);

                connection.setAutoCommit(false);

                try (ResultSet resultSet = psSelectColumn.executeQuery()) {

                    while (resultSet.next()) {

                        result.add(new CellDTO(resultSet.getInt("rowId"), columnId, resultSet.getString("value")));

                    }
                }
                connection.commit();
            }
        }
        return result;
    }


    /**
     * The method take column's value
     *
     * @param rowId    row's id
     * @param columnId column's name
     * @return value column
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public List<CellDTO> getCellValue(int rowId, int columnId) throws SQLException {

        List<CellDTO> result = new LinkedList<>();
        try (Connection connection = connectionPool.getConnection()) {
            if (!verifyRowId(rowId)) {

                return result;
            }

            if (!verifyCell(rowId, columnId)) {
                return result;
            }

            String selectColumnValue = properties.getProperty("get.column_value");

            try (PreparedStatement psSelectColumnValue = connection.prepareStatement(selectColumnValue)) {

                psSelectColumnValue.setInt(1, rowId);
                psSelectColumnValue.setInt(2, columnId);

                try (ResultSet resultSet = psSelectColumnValue.executeQuery()) {

                    if (resultSet.next()) {

                        result.add(new CellDTO(rowId, columnId, resultSet.getString("value")));
                    }
                }
            }
        }
        return result;
    }


    /**
     * The method create column
     *
     * @param rowId    row's id
     * @param columnId column's id
     * @param value    value column
     * @return message about successfully create column
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean createCell(int rowId, int columnId, String value) throws SQLException {

        try (Connection connection = connectionPool.getConnection()) {
            if (verifyCell(rowId, columnId)) {
                return false;
            }

            String selectCreateColumn = properties.getProperty("create.column");

            try (PreparedStatement psCreateColumn = connection.prepareStatement(selectCreateColumn)) {

                connection.setAutoCommit(false);
                psCreateColumn.setInt(1, rowId);
                psCreateColumn.setInt(2, columnId);
                psCreateColumn.setString(3, value);

                psCreateColumn.execute();
                connection.commit();
            }
        }
        return true;
    }


    /**
     * The method delete cell
     *
     * @param rowId    line's id
     * @param columnId column's name
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean removeCell(int rowId, int columnId) throws SQLException {

        try (Connection connection = connectionPool.getConnection()) {

            if (!verifyRowId(rowId) || !verifyCell(rowId, columnId)) return false;

            String queryDeleteColumn = properties.getProperty("delete.cell");

            try (PreparedStatement psDeleteColumn = connection.prepareStatement(queryDeleteColumn)) {

                psDeleteColumn.setInt(1, rowId);
                psDeleteColumn.setInt(2, columnId);

                connection.setAutoCommit(false);

                psDeleteColumn.executeUpdate();

                connection.commit();
            }
        }
        return true;
    }


    /**
     * The method delete data about row from DB
     *
     * @param rowId row's id
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean removeRow(int rowId) throws SQLException {

        try (Connection connection = connectionPool.getConnection()) {

            if (!verifyRowId(rowId)) return false;

            String queryDeleteRow = properties.getProperty("delete.row");

            try (PreparedStatement psDeleteLine = connection.prepareStatement(queryDeleteRow)) {

                psDeleteLine.setInt(1, rowId);

                connection.setAutoCommit(false);

                psDeleteLine.executeUpdate();

                connection.commit();
            }
        }
        return true;
    }


    /**
     * The method delete data about column from DB
     *
     * @param columnId column's id
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean removeColumn(int columnId) throws SQLException {

        int amountDeleteCell;

        try (Connection connection = connectionPool.getConnection()) {

            String queryDeleteColumn = properties.getProperty("delete.column");

            try (PreparedStatement psDeleteColumns = connection.prepareStatement(queryDeleteColumn)) {

                psDeleteColumns.setInt(1, columnId);

                connection.setAutoCommit(false);

                amountDeleteCell = psDeleteColumns.executeUpdate();

                connection.commit();
            }
        }
        return amountDeleteCell > 0;
    }


    /**
     * The method change value in column
     *
     * @param columnId          column's id
     * @param newColumnValue    new value column
     * @return  change successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean setColumn(int columnId, String newColumnValue) throws SQLException{

        int result = -1;
        try (Connection connection = connectionPool.getConnection()) {

            String lockColumn = properties.getProperty("lock.column");
            String setColumn = properties.getProperty("set.column");

            try (PreparedStatement psLockColumn = connection.prepareStatement(lockColumn); PreparedStatement psSetColumn = connection.prepareStatement(setColumn)) {

                psLockColumn.setInt(1, columnId);
                psSetColumn.setString(1, newColumnValue);
                psSetColumn.setInt(2, columnId);

                connection.setAutoCommit(false);

                try (ResultSet resultSet = psLockColumn.executeQuery()) {

                    if (resultSet.next()) {

                        result = psSetColumn.executeUpdate();
                    }
                }
                connection.commit();
            }
        }
        return result > 0;
    }


    /**
     * The method change cell's value
     *
     * @param rowId              row's id
     * @param columnId           column's id
     * @param newCellValue       new cell's value
     * @return  change successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean setCellValue(int rowId, int columnId, String newCellValue) throws SQLException{

        int result = -1;
        try (Connection connection = connectionPool.getConnection()) {

            if (!verifyCell(rowId, columnId)) {
                return false;
            }
            String lockCell = properties.getProperty("lock.cell");
            String setCell = properties.getProperty("set.cell");

            try (PreparedStatement psLockCell = connection.prepareStatement(lockCell); PreparedStatement psSetCellValue = connection.prepareStatement(setCell)) {

                psLockCell.setInt(1, rowId);
                psLockCell.setInt(2, columnId);
                psSetCellValue.setString(1, newCellValue);
                psSetCellValue.setInt(2, rowId);
                psSetCellValue.setInt(3, columnId);

                connection.setAutoCommit(false);

                try (ResultSet resultSet = psLockCell.executeQuery()) {

                    if (resultSet.next()) {

                        result = psSetCellValue.executeUpdate();
                    }
                }
                connection.commit();
            }
        }
        return result > 0;
    }


    /**
     * The method change row's value
     *
     * @param rowId        row's id
     * @param newValue      new row's value
     * @return    change successfully or not
     * @throws SQLException  if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public boolean setRow(int rowId, String newValue) throws SQLException{

        int result = -1;
        try (Connection connection = connectionPool.getConnection()){

            if (!verifyRowId(rowId)) {
                return false;
            }
                String lockRow = properties.getProperty("lock.row");
                String setRow = properties.getProperty("set.row");

                try (PreparedStatement psLockRow = connection.prepareStatement(lockRow); PreparedStatement psSetRow = connection.prepareStatement(setRow)){

                    psLockRow.setInt(1, rowId);
                    psSetRow.setString(1, newValue);
                    psSetRow.setInt(2, rowId);

                    connection.setAutoCommit(false);

                    try (ResultSet resultSet = psLockRow.executeQuery()){

                        if (resultSet.next()){

                            result = psSetRow.executeUpdate();
                        }
                    }
                    connection.commit();
                }
        }
        return result > 0;
    }


    /**
     * The method find cell with user's value
     *
     * @param value             user's value
     * @return                  collection with cells which have this value
     * @throws SQLException  if some thing wrong with working DB
     */
    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = "SQL_PREPARED_STATEMENT_GENERATED_FROM_NONCONSTANT_STRING", justification = "Exception detail hide")
    public List<CellDTO> findValue(String value) throws SQLException {

        String findValue = properties.getProperty("find.value");
        List<CellDTO> result = new LinkedList<>();

        try (Connection connection = connectionPool.getConnection(); PreparedStatement psFindValue = connection.prepareStatement(findValue)){

            psFindValue.setString(1, value);

            try (ResultSet resultSet = psFindValue.executeQuery()){

                while (resultSet.next()){

                    result.add(new CellDTO(resultSet.getInt("rowId"), resultSet.getInt("columnId"), value));
                }
            }
        }
        return result;
    }

}
