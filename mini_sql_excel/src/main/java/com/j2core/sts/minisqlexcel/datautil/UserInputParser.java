package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.Action;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by sts on 8/22/16.
 */

/**
 * Class parsed user selects
 */
public class UserInputParser {

    /**
     * Constructor
     */
    public UserInputParser() {
    }


    /**
     * The method pars user's select
     *
     * @param userInput    string with user's select
     * @return  object with information from user's select
     * @throws IOException           if problem with reade property's file
     * @throws ParseProcessException if some data aren't integer
     * @throws SQLException          if some thing wrong with DB
     */
    public UserDataDTO parseUserInput(String userInput) throws ParseProcessException, IOException, SQLException {

        UserDataDTO result;
        int indexFirst = 0;
        int indexSecond;
        indexSecond = userInput.indexOf(' ');

        if (indexSecond > 0) {
            String action = userInput.substring(indexFirst, indexSecond);
            result = new UserDataDTO(parseUserAction(action));
            indexFirst = indexSecond+1;

            indexSecond = userInput.indexOf(' ', indexFirst);
            if (indexSecond > 0){

                result = parseCellData(userInput.substring(indexFirst, indexSecond), result);
                result.setValue(userInput.substring(indexSecond+1));

            }else {

                result = parseCellData(userInput.substring(indexFirst), result);
            }
        }else {

            Action action = parseUserAction(userInput);
            if (action != null){

                result = new UserDataDTO(action);

            }else throw new ParseProcessException();

        }

        return result;
    }


    /**
     * The method spot user's action in his select
     *
     * @param userQuery  data with user's action
     * @return  user's action in his select or null
     */
    private Action parseUserAction(String userQuery){

        for (Action action : Action.values()){

            if (action.getAction().equals(userQuery)){
                return action;
            }
        }
        return null;
    }


    /**
     * The method parse information about cell
     *
     * @param cellData information about cell
     * @param userData user's data
     * @return corrected user's data
     * @throws IOException           if problem with reade property's file
     * @throws ParseProcessException if some data aren't integer
     * @throws SQLException          if some thing wrong with DB
     */
    public UserDataDTO parseCellData(String cellData, UserDataDTO userData) throws IOException, ParseProcessException, SQLException {

        Enum action = userData.getAction();
        if (action == Action.CALCULATE) {

            if (cellData.charAt(0) == '=') {

                userData.setValue(cellData.substring(1));
            } else {

                userData.setValue(cellData);
            }
        } else if (action == Action.FIND) {

            userData.setValue(cellData);

        } else {
            try {
                int index = -1;
                for (int i = 0; i < cellData.length(); i++) {

                    if (!Character.isLetter(cellData.charAt(i))) {
                        index = i;
                        break;
                    }
                }
                if (index < 0) {
                    userData.setColumnId(ConverterColumnId.convertColumnName(cellData));

                } else if (index > 0 && index < cellData.length()) {

                    userData.setRowId(Integer.parseInt(cellData.substring(index)));
                    userData.setColumnId(ConverterColumnId.convertColumnName(cellData.substring(0, index)));

                } else {
                    userData.setRowId(Integer.parseInt(cellData));
                }
            } catch (NumberFormatException ex) {

                throw new ParseProcessException();
            }
        }
        return userData;
    }

}
