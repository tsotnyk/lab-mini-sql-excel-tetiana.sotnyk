package com.j2core.sts.minisqlexcel.datautil.exception;

/**
 * Created by sts on 8/7/16.
 */

/**
 * The exception class which throw if some thing wrong with expression processing
 */
public class ExpressionProcessException extends Exception {

    /**
     * Constructor
     */
    public ExpressionProcessException(){
        super();
    }
}
