package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dto.CellDTO;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;

import java.util.List;

/**
 * Created by sts on 9/8/16.
 */

/**
 * The class create output about results user's actions with data from DB
 */
public class CreatorUserOutput {

    /**
     * The method create user's output about get data from DB
     *
     * @param userData         user's  data
     * @param cellDTOList      information from DB
     * @return                 user's output
     */
    public static String createUserOutputGetData(UserDataDTO userData, List<CellDTO> cellDTOList){

        String columnName = null;

        if (cellDTOList.isEmpty()){
            return " Data isn't exist.";
        }

        if (userData.getColumnId() > 0){

            columnName = ConverterColumnId.convertColumnId(userData.getColumnId());
        }

        StringBuilder userOutput = new StringBuilder();
        userOutput.append(" Date in ");

        if (userData.getRowId() > 0 && userData.getColumnId() > 0) {

            userOutput.append(columnName).append(userData.getRowId()).append(" = ");
            userOutput.append(cellDTOList.get(0).getValue());

        } else if (userData.getColumnId() < 0) {

            userOutput.append(userData.getRowId()).append(" row is");

            for (CellDTO cell : cellDTOList) {

                columnName = ConverterColumnId.convertColumnId(cell.getColumnId());
                userOutput.append(" column ").append(columnName).append(" = ").append(cell.getValue()).append(";");

            }
        } else {

            userOutput.append(columnName).append(" column is ");

            for (CellDTO cell : cellDTOList) {

                userOutput.append(" row ").append(cell.getRowId()).append(" = ").append(cell.getValue());

            }
        }

        return userOutput.toString();
    }


    /**
     * The method create user's output about remove data from DB
     *
     * @param userData       user's data
     * @param result         result remove data
     * @return               user's output
     */
    public  static String createUserOutputRemoveData(UserDataDTO userData, boolean result){

        StringBuilder userOutput = new StringBuilder();
        userOutput.append(" Date in ");
        String columnName = null;

        if (userData.getColumnId() > 0){
            columnName = ConverterColumnId.convertColumnId(userData.getColumnId());
        }

        if (userData.getRowId() > 0 && userData.getColumnId() > 0) {

            userOutput.append(columnName).append(userData.getRowId());

        } else if (userData.getColumnId() < 0) {

            userOutput.append(userData.getRowId()).append(" row ");

        } else {

            userOutput.append(columnName).append(" column ");

        }

        if (result){

            userOutput.append(" remove successfully.");

        }else {

            userOutput.append(" remove unsuccessfully.");

        }

        return userOutput.toString();
    }


    /**
     * The method create user's output about set data in DB
     *
     * @param userData        user's data
     * @param result          result set data in DB
     * @param flagExist       exist or not data in DB
     * @return                user's output
     */
    public  static String createUserOutputSetData(UserDataDTO userData, boolean result, boolean flagExist){

        StringBuilder userOutput = new StringBuilder();
        String columnName = null;

        if (userData.getColumnId() > 0){

            columnName = ConverterColumnId.convertColumnId(userData.getColumnId());
        }

        if (flagExist){

            userOutput.append(" Change data in ");

            if (userData.getRowId() > 0 && userData.getColumnId() > 0) {

                userOutput.append(columnName).append(userData.getRowId());

            } else if (userData.getColumnId() < 0) {

                userOutput.append(userData.getRowId()).append(" row");

            } else {
                userOutput.append(columnName).append(" column");

            }
        }else {
            userOutput.append(" Data in cell ").append(columnName).append(userData.getRowId()).append(" isn't exist. Create new cell");

        }
        if (result){
            userOutput.append(" is successfully.");

        }else {
            userOutput.append(" is unsuccessfully.");

        }
        return userOutput.toString();
    }


    /**
     * The method create user's output about calculate expression
     *
     * @param userData          user's data
     * @param result            result calculation
     * @return                  user's output
     */
    public  static String createUserOutputCalculateExpression(UserDataDTO userData, Double result){

        return "  Expression " + userData.getValue() + " = " + result + ".";
    }


    /**
     * The method create user's output about not exist data in row or column for calculation
     *
     * @param userData     user's data
     * @return             user's output
     */
    public  static String createUserOutputNotData(UserDataDTO userData) {

        return " Data in " + userData.getValue() + " aren't exist.";

    }


    /**
     * The method create user's output about cells which have user's value
     *
     * @param userData          user's data
     * @param cellDTOList       collection with cells which have user's value
     * @return                  user's output
     */
    public  static String createUserOutputFindValue(UserDataDTO userData, List<CellDTO> cellDTOList){

        StringBuilder result = new StringBuilder();
        result.append(" The desired value = ").append(userData.getValue()).append(" is in: ");

        for (CellDTO cell : cellDTOList){

            result.append(ConverterColumnId.convertColumnId(cell.getColumnId())).append(cell.getRowId()).append("; ");

        }

        return result.toString();

    }

}
