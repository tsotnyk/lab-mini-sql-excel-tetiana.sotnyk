package com.j2core.sts.minisqlexcel.dao;

import com.j2core.sts.minisqlexcel.dto.CellDTO;
import com.j2core.sts.minisqlexcel.dto.CellId;
import com.j2core.sts.minisqlexcel.util.CloseableHibernateSession;
import com.j2core.sts.minisqlexcel.util.DBSessionProvider;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sts on 9/29/16.
 */
public class HibernateWorkerDAO implements DBWorkerDAO{

    private final static Logger LOGGER = LoggerFactory.getLogger(DBSessionProvider.class);


    public HibernateWorkerDAO() {
    }


    @Override
    public List<CellDTO> getRow(int rowId) throws SQLException {

        Transaction transaction;
        List<CellDTO> result = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            Criteria criteria = closeableSession.getSession().createCriteria(CellDTO.class);
            criteria.add(Restrictions.eq("rowId", rowId));

            result = (List<CellDTO>) criteria.list();

            transaction.commit();

        } catch (Exception e) {
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public List<CellDTO> getCellValue(int rowId, int columnId) throws SQLException {

        Transaction transaction;
        List<CellDTO> result = new LinkedList<>();
        CellDTO cell = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            cell = (CellDTO) closeableSession.getSession().get(CellDTO.class, new CellId(rowId, columnId));

            transaction.commit();
        } catch (Exception e) {
            LOGGER.error(e + " Sorry.");
        }

        if (cell != null) {

            result.add(cell);
        }

        return result;
    }


    @Override
    public List<CellDTO> getColumn(int columnId) throws SQLException {

        Transaction transaction;
        List<CellDTO> result = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            Criteria criteria = closeableSession.getSession().createCriteria(CellDTO.class);
            criteria.add(Restrictions.eq("columnId", columnId));

            result = (List<CellDTO>) criteria.list();

            transaction.commit();
        } catch (Exception e) {
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public boolean createCell(int rowId, int columnId, String value) throws SQLException {

        boolean result = true;
        CellDTO cell = new CellDTO(rowId, columnId, value);
        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            closeableSession.getSession().save(cell);

            transaction.commit();

        } catch (Exception e) {
            result = false;
            LOGGER.error(e + " Sorry, Can't create cell.");
        }

        return result;
    }


    @Override
    public boolean verifyRowId(int rowId) throws SQLException {
        boolean result = true;
        List<CellDTO> cell = getRow(rowId);

        if (cell.isEmpty()) {
            result = false;
        }

        return result;
    }


    @Override
    public boolean verifyCell(int rowId, int columnId) throws SQLException {

        boolean result = true;
        List<CellDTO> cell = getCellValue(rowId, columnId);

        if (cell.isEmpty()) {
            result = false;
        }

        return result;
    }


    @Override
    public List<CellDTO> findValue(String value) throws SQLException {

        Transaction transaction;
        List<CellDTO> result = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            Criteria criteria = closeableSession.getSession().createCriteria(CellDTO.class);
            criteria.add(Restrictions.eq("value", value));

            result = (List<CellDTO>) criteria.list();

            transaction.commit();
        } catch (Exception e) {
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public boolean removeCell(int rowId, int columnId) throws SQLException {

        boolean result = true;
        Transaction transaction;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            CellDTO cell = new CellDTO();
            cell.setRowId(rowId);
            cell.setColumnId(columnId);

            closeableSession.getSession().delete(cell);

            transaction.commit();

        } catch (Exception e) {
            result = false;
            LOGGER.error(e + " Can'n remove object");
        }

        return result;

    }


    @Override
    public boolean removeRow (int rowId){

        boolean result = true;
        Transaction transaction = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = closeableSession.getSession().beginTransaction();

            String deleteString = "delete from CellDTO where rowId = :valueRowId";
            Query deleteQuery = closeableSession.getSession().createQuery(deleteString);
            deleteQuery.setInteger("valueRowId", rowId);

            deleteQuery.executeUpdate();

            transaction.commit();

        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            result = false;
            LOGGER.error(e + " Sorry.");
        }

        return result;

    }


    @Override
    public boolean removeColumn(int columnId) throws SQLException {

        boolean result = true;
        Transaction transaction = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())){

            transaction = closeableSession.getSession().beginTransaction();

            String deleteString = "delete from CellDTO where columnId = :valueColumnId";
            Query deleteQuery = closeableSession.getSession().createQuery(deleteString);
            deleteQuery.setInteger("valueColumnId", columnId);

            deleteQuery.executeUpdate();

            transaction.commit();
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }
            result = false;
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public boolean setColumn(int columnId, String newColumnValue) throws SQLException {

        boolean result = true;
        Transaction transaction = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            Query setQuery = closeableSession.getSession().createQuery("UPDATE CellDTO set value = :value WHERE columnId = :columnId");
            setQuery.setParameter("value", newColumnValue);
            setQuery.setParameter("columnId", columnId);

            setQuery.executeUpdate();

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            result = false;
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public boolean setRow(int rowId, String newValue) throws SQLException {

        boolean result = true;
        Transaction transaction = null;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            Query setQuery = closeableSession.getSession().createQuery("UPDATE CellDTO set value = :value WHERE rowId = :rowId");
            setQuery.setParameter("value", newValue);
            setQuery.setParameter("rowId", rowId);

            setQuery.executeUpdate();

            transaction.commit();

        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            result = false;
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }


    @Override
    public boolean setCellValue(int rowId, int columnId, String newValue) throws SQLException {

        Transaction transaction;
        boolean result = true;

        try (CloseableHibernateSession closeableSession = new CloseableHibernateSession(DBSessionProvider.getProviderInstance().getSessionFactory().openSession())) {

            transaction = closeableSession.getSession().beginTransaction();

            CellDTO cell = new CellDTO(rowId, columnId, newValue);

            closeableSession.getSession().update(cell);

            transaction.commit();

        } catch (Exception e) {
            result = false;
            LOGGER.error(e + " Sorry.");
        }

        return result;
    }

}
