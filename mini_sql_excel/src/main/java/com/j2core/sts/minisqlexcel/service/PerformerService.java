package com.j2core.sts.minisqlexcel.service;

import com.j2core.sts.minisqlexcel.dto.UserDataDTO;

import java.io.IOException;

/**
 * Created by sts on 8/25/16.
 */

/**
 * Interface for work with user's data
 */
public interface PerformerService {

    /**
     * The method processing user input
     *
     * @param userData   data with user's input
     * @return    message for user about his input
     * @throws IOException if some thing wrong with properties file
     */
    String processUserInput(UserDataDTO userData) throws IOException;
}
