package com.j2core.sts.minisqlexcel.service;

import com.j2core.sts.minisqlexcel.datautil.CreatorUserOutput;
import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.DataParser;
import com.j2core.sts.minisqlexcel.datautil.UserInputParser;
import com.j2core.sts.minisqlexcel.datautil.VerifierData;
import com.j2core.sts.minisqlexcel.datautil.exception.ExpressionProcessException;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.Action;
import com.j2core.sts.minisqlexcel.dto.CellDTO;
import com.j2core.sts.minisqlexcel.dto.UserDataDTO;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by sts on 8/25/16.
 */

/**
 * The class process user input and perform user action
 */
public class DefaultPerformerService implements PerformerService {

    private static final Logger LOGGER = Logger.getLogger(DefaultPerformerService.class); // object for save log information
    private VerifierData verifierData;                                                    // object for verify data
    private CalculatorService calculator;                                                 // object for calculate expression
    private DBWorkerDAO dbWorker;
    private UserInputParser inputParser;


    /**
     * Constructor
     *
     * @param dbWorker       object for work with DB
     * @param inputParser    object for parse user's input
     * @param dataParser     object for parse user's data
     * @param verifierData   object for verify user's data
     */
    public DefaultPerformerService(DBWorkerDAO dbWorker, UserInputParser inputParser, DataParser dataParser, VerifierData verifierData) {
        this.dbWorker = dbWorker;
        this.inputParser = inputParser;
        this.calculator = new CalculatorService(dbWorker, dataParser);
        this.verifierData = verifierData;

    }


    @Override
    public String processUserInput(UserDataDTO userData) throws IOException {

        String result;
        try {
            Action action = (Action) userData.getAction();
            switch (action) {
                case HELP:
                    result = "Help!";//TODO
                    break;
                case GET:
                    result = getData(userData);
                    break;
                case CALCULATE:
                    result = calculateData(userData);
                    break;
                case SET:
                    result = setData(userData);
                    break;
                case REMOVE:
                    result = removeData(userData);
                    break;
                case FIND:
                    result = findData(userData);
                    break;
                default:
                    result = "Sorry, some thing wrong with your data. Please try again.";
                    break;
            }
        } catch (SQLException | ParseProcessException e) {
            LOGGER.error(e);
            result = "Sorry, some thing wrong with DB. Please try again.";
        }
        return result;
    }


    /**
     * The method find cells which have user's value
     *
     * @param userData     user's data about user's value
     * @return             data with information
     * @throws SQLException if some thing wrong with working DB
     */
    private String findData(UserDataDTO userData) throws SQLException{

        List<CellDTO> cellDTOList = dbWorker.findValue(userData.getValue());
        if (cellDTOList.isEmpty()){
            return CreatorUserOutput.createUserOutputNotData(userData);
        }else {
            return CreatorUserOutput.createUserOutputFindValue(userData, cellDTOList);
        }
    }


    /**
     * The method get data from DB
     *
     * @param userData user's data about which cell you need
     * @return data with information
     * @throws SQLException if some thing wrong with working DB
     * @throws IOException                if come thong wrong with properties file
     * @throws ParseProcessException      if come thong wrong with data
     */
    public String getData(UserDataDTO userData) throws SQLException, ParseProcessException, IOException {

        List<CellDTO> cellDTOList;
        if (userData.getRowId() != -1 && userData.getColumnId() != -1) {

            cellDTOList = dbWorker.getCellValue(userData.getRowId(), userData.getColumnId());

        } else if (userData.getRowId() != -1) {

            cellDTOList = dbWorker.getRow(userData.getRowId());

        } else {

            cellDTOList = dbWorker.getColumn(userData.getColumnId());
        }

        for (CellDTO  cellDTO : cellDTOList){

            if (cellDTO.getValue().charAt(0) == '='){

                UserDataDTO newAction = new UserDataDTO(Action.CALCULATE);
                newAction.setRowId(cellDTO.getRowId());
                newAction.setColumnId(cellDTO.getColumnId());
                newAction.setValue(cellDTO.getValue().substring(1));
                cellDTO.setValue(calculateData(newAction));
            }
        }

        return CreatorUserOutput.createUserOutputGetData(userData, cellDTOList);
    }


    /**
     * The method remove data from DB
     *
     * @param userData user's data about which cell you need
     * @return information about remove user's data
     * @throws SQLException if some thing wrong with working DB
     */
    public String removeData(UserDataDTO userData) throws SQLException {

        boolean result;
        if (userData.getRowId() != -1 && userData.getColumnId() != -1) {

            result = dbWorker.removeCell(userData.getRowId(), userData.getColumnId());

        } else if (userData.getRowId() != -1) {

            result = dbWorker.removeRow(userData.getRowId());

        } else {

            result = dbWorker.removeColumn(userData.getColumnId());
        }

        return CreatorUserOutput.createUserOutputRemoveData(userData, result);
    }


    /**
     * The method change user's data in DB, if need do with cell, and like this cell isn't cell create
     *
     * @param userData user's data for change data in DB
     * @return information about change data
     * @throws SQLException if some thing wrong with working DB
     */
    public String setData(UserDataDTO userData) throws SQLException {

        if (userData.getValue() == null){
            return "Sorry, some thing wrong with your data. Please try again.";
        }

        boolean result;
        boolean flagExist = true;
        if (userData.getRowId() != -1 && userData.getColumnId() != -1) {

            if (dbWorker.verifyCell(userData.getRowId(), userData.getColumnId())) {

                result = dbWorker.setCellValue(userData.getRowId(), userData.getColumnId(), userData.getValue());

            } else {

                flagExist = false;
                result = dbWorker.createCell(userData.getRowId(), userData.getColumnId(), userData.getValue());

            }
        } else if (userData.getRowId() != -1) {

            result = dbWorker.setRow(userData.getRowId(), userData.getValue());

        } else {

            result = dbWorker.setColumn(userData.getColumnId(), userData.getValue());
        }

        return CreatorUserOutput.createUserOutputSetData(userData, result, flagExist);
    }


    /**
     * The method calculate user's expression
     *
     * @param userData                   information about user's expression
     * @return                           expression's answer
     * @throws SQLException              if some thing wrong with working DB
     * @throws ParseProcessException     if expression isn't valid
     */
    public String calculateData(UserDataDTO userData) throws SQLException, ParseProcessException, IOException {

        String expression = userData.getValue();
        if (verifierData.verifyRowOrColumnCalculation(expression)) {

            expression = createNewExpression(expression);
            if (expression == null) {
                return CreatorUserOutput.createUserOutputNotData(userData);
            }
        }
        try {
            if (verifierData.verificationExpression(expression) && verifierData.isBracketsValid(expression)) {

                expression = verifierData.trimSpace(expression);
                return CreatorUserOutput.createUserOutputCalculateExpression(userData, calculator.calculateExpression(expression));
            }
        } catch (ExpressionProcessException e) {
            LOGGER.info(e);
        }

        return CreatorUserOutput.createUserOutputCalculateExpression(userData, null);

    }


    /**
     * The method create new expression with data from user's row or column
     *
     * @param oldExpression               original expression
     * @return                            new expression with data from user's row or column
     * @throws IOException                if come thong wrong with properties file
     * @throws ParseProcessException      if come thong wrong with original expression
     * @throws SQLException               if some thing wrong with working DB
     */
    public String createNewExpression(String oldExpression) throws IOException, ParseProcessException, SQLException {

        List<CellDTO> cellDTOList;
        UserDataDTO userDataDTO = new UserDataDTO(Action.GET);
        String expression = oldExpression;
        if (oldExpression.charAt(0) == '='){
            expression = oldExpression.substring(1);
        }

        userDataDTO = inputParser.parseCellData(expression.substring(0, expression.length()-2), userDataDTO);
        if (userDataDTO.getRowId() < 0){
            cellDTOList = dbWorker.getColumn(userDataDTO.getColumnId());
        }else {
            cellDTOList = dbWorker.getRow(userDataDTO.getRowId());
        }
        if (cellDTOList.isEmpty()){
            return null;
        }else {
            if (cellDTOList.size() == 1){
                expression = cellDTOList.get(0).getValue();
            }else {
                StringBuilder stringBuilder = new StringBuilder();
                int i;
                for (i = 0; i < cellDTOList.size()-1; i++){
                    stringBuilder.append(cellDTOList.get(i).getValue()).append('+');
                }
                stringBuilder.append(cellDTOList.get(i).getValue());
                expression = stringBuilder.toString();
            }
        }
        return  expression;
    }

}
