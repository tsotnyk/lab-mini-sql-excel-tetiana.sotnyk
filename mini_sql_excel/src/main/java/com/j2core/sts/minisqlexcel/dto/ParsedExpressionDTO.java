package com.j2core.sts.minisqlexcel.dto;

import java.util.List;

/**
 * Created by sts on 8/4/16.
 */

/**
 * The class for save characters collection with expression's actions and string collection with expression's numbers
 */
public class ParsedExpressionDTO {

    private List<Character> actions;
    private List<String> cells;

    /**
     * Constructor
     *
     * @param actions collection with expression's actions
     * @param cells   collection with expression's numbers
     */
    public ParsedExpressionDTO(List<Character> actions, List<String> cells){

        this.actions = actions;
        this.cells = cells;
    }

    public List<String> getCells() {
        return cells;
    }

    public List<Character> getActions() {
        return actions;
    }

    public void setActions(List<Character> actions) {
        this.actions = actions;
    }

    public void setCells(List<String> cells) {
        this.cells = cells;
    }
}
