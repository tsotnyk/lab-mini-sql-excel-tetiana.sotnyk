package com.j2core.sts.minisqlexcel.dto;


/**
 * Created by sts on 8/22/16.
 */

/**
 * The class for save information about user's select
 */
public class UserDataDTO {

    private Enum action;                    // user's action in his select
    private int rowId = -1;                      // row's id
    private int columnId = -1;                   // column's id
    private String value;                   // cell's value

    /**
     * Constructor for class UserDataDTO
     *
     * @param action       user's action ih his select
     */
    public UserDataDTO(Enum action) {
        this.action = action;
    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Enum getAction() {
        return action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDataDTO dataDTO = (UserDataDTO) o;

        if (rowId != dataDTO.rowId) return false;
        if (columnId != dataDTO.columnId) return false;
        if (!action.equals(dataDTO.action)) return false;
        if (value != null && dataDTO.value != null){
            return value.equals(dataDTO.value);
        }else if (value == null && dataDTO.value == null){
            return true;
        }else return false;
    }

    @Override
    public int hashCode() {
        int result = action.hashCode();
        result = 31 * result + rowId;
        result = 31 * result + (int)columnId;
        return result;
    }
}
