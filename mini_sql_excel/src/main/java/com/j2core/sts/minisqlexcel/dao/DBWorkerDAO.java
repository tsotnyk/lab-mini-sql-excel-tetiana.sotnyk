package com.j2core.sts.minisqlexcel.dao;

import com.j2core.sts.minisqlexcel.dto.CellDTO;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by sts on 8/25/16.
 */

/**
 * Interface for ork with data bases
 */
public interface DBWorkerDAO {

    /**
     * The method take information about row
     *
     * @param rowId row's id
     * @return information about row
     * @throws SQLException if some thing wrong with working DB
     */
    List<CellDTO> getRow(int rowId) throws SQLException;

    /**
     * The method take column's value
     *
     * @param rowId    row's id
     * @param columnId column's name
     * @return value column
     * @throws SQLException if some thing wrong with working DB
     */
    List<CellDTO> getCellValue(int rowId, int columnId) throws SQLException;

    /**
     * The method take information about column
     *
     * @param columnId column's id
     * @return information about column
     * @throws SQLException if some thing wrong with working DB
     */
    List<CellDTO> getColumn(int columnId) throws SQLException;

    /**
     * The method create column
     *
     * @param rowId    row's id
     * @param columnId column's id
     * @param value    value column
     * @return message about successfully create column
     * @throws SQLException if some thing wrong with working DB
     */
    boolean createCell(int rowId, int columnId, String value) throws SQLException;

    /**
     * The method verification row's id
     *
     * @param rowId row's name
     * @return exist or not exist this row's id in DB
     * @throws SQLException if some thing wrong with working DB
     */
    boolean verifyRowId(int rowId) throws SQLException;

    /**
     * The method verification column's name
     *
     * @param rowId    row's id
     * @param columnId column's name
     * @return exist or not exist this column's name for this row in DB
     * @throws SQLException if some thing wrong with working DB
     */
    boolean verifyCell(int rowId, int columnId) throws SQLException ;

    /**
     * The method delete data about row from DB
     *
     * @param rowId row's id
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    boolean removeRow(int rowId) throws SQLException;

    /**
     * The method delete cell
     *
     * @param rowId    line's id
     * @param columnId column's name
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    boolean removeCell(int rowId, int columnId) throws SQLException;

    /**
     * The method delete data about column from DB
     *
     * @param columnId column's id
     * @return delete successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    boolean removeColumn(int columnId) throws SQLException;

    /**
     * The method change value in column
     *
     * @param columnId          column's id
     * @param newColumnValue    new value column
     * @return  change successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    boolean setColumn(int columnId, String newColumnValue) throws SQLException;

    /**
     * The method change cell's value
     *
     * @param rowId              row's id
     * @param columnId           column's id
     * @param newValue       new cell's value
     * @return  change successfully or not
     * @throws SQLException if some thing wrong with working DB
     */
    boolean setCellValue(int rowId, int columnId, String newValue) throws SQLException;

    /**
     * The method change row's value
     *
     * @param rowId        row's id
     * @param newValue      new row's value
     * @return    change successfully or not
     * @throws SQLException  if some thing wrong with working DB
     */
    boolean setRow(int rowId, String newValue) throws SQLException;


    /**
     * The method find cell with user's value
     *
     * @param value             user's value
     * @return                  collection with cells which have this value
     * @throws SQLException  if some thing wrong with working DB
     */
    List<CellDTO> findValue(String value) throws SQLException;

}

