package com.j2core.sts.minisqlexcel.datautil;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.exception.ParseProcessException;
import com.j2core.sts.minisqlexcel.dto.ParsedExpressionDTO;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 * The class parsing data
 */
public class DataParser {

    private static final Logger LOGGER = Logger.getLogger(DataParser.class);  //class fos save log information

    /**
     * Constructor
     */
    public DataParser(){
    }


    /**
     * The method parsing for determine this string is number or no
     *
     * @param value   string with data
     * @return        value numbers
     * @throws ParseProcessException if this string isn't number
     */
    public double parseNumber(String value) throws ParseProcessException {

        double result;
        int index = 0;
        int point = 0;
        if (value.charAt(0) == '-') {
            index++;
        }
        while (index < value.length() && (Character.isDigit(value.charAt(index)) || value.charAt(index) == '.')) {

                if (value.charAt(index) == '.' && ++point > 1) {
                    LOGGER.error("Number " + value + "is not correctly");
                    throw  new ParseProcessException();
                }
                index++;
        }

        try {
            result = Double.parseDouble(value);
        }catch (NumberFormatException ex){
            LOGGER.info(ex);
            throw new ParseProcessException();
        }
        return result;
    }


    /**
     * The method parsing expression, numbers in the value collection and actions in the actions collection
     *
     * @param expression   expression
     * @return             object with number's and action's collections
     * @throws ParseProcessException   if this expression has mistake
     */
    public ParsedExpressionDTO parseExpression(String expression) throws ParseProcessException {

        List<String> collectionValue = new LinkedList<>();
        List<Character> collectionAction = new LinkedList<>();
        int indexExpression = 0;
        int indexValue = 0;

        if (expression.charAt(indexExpression) == '+' || expression.charAt(indexExpression) == '-') indexExpression++;

        while (indexExpression < expression.length()){

            if (expression.charAt(indexExpression) == '+' || expression.charAt(indexExpression) == '-' || expression.charAt(indexExpression) == '*' ||
                    expression.charAt(indexExpression) == '/'){
                collectionValue.add(expression.substring(indexValue, indexExpression));
                collectionAction.add(expression.charAt(indexExpression));
                indexValue = indexExpression +1;
                if (expression.charAt(indexExpression+1) == '+' || expression.charAt(indexExpression+1) == '-') {
                    indexExpression++;
                }
            }

            indexExpression++;
        }

        if (indexExpression == indexValue) {
            throw new ParseProcessException();
        }else {
            collectionValue.add(expression.substring(indexValue));
        }

        if (collectionAction.size() == collectionValue.size()) throw new ParseProcessException();

        return  new ParsedExpressionDTO(collectionAction, collectionValue);
    }


    /**
     * The method changed variables on the value cells from DB
     *
     * @param parsedExpression  parsed expression
     * @return  changed parsed expression (this parsed expression has only numbers and actions, and ready for calculate)
     */
    public ParsedExpressionDTO substitutionData(ParsedExpressionDTO parsedExpression, DBWorkerDAO worker) throws ParseProcessException, SQLException {

        int rowId;
        int columnId;
        List<String> newCollectionValue = new LinkedList<>();
        List<String> collectionValue = parsedExpression.getCells();

        for (String value : collectionValue) {

            int indexColumn = -1;
            for (int i = 0; i < value.length(); i++) {

                if (!Character.isLetter(value.charAt(i))) {
                    indexColumn = i;
                    break;
                }
            }
            if (indexColumn == 0) {
                newCollectionValue.add(value);
            } else {
                try {

                    rowId = Integer.parseInt(value.substring(indexColumn));
                    columnId = ConverterColumnId.convertColumnName(value.substring(0, indexColumn));

                } catch (NumberFormatException ex) {
                    throw new ParseProcessException();
                }

                newCollectionValue.add(worker.getCellValue(rowId, columnId).get(0).getValue());
            }
        }

        parsedExpression.setCells(newCollectionValue);

        return parsedExpression;
    }


    /**
     * The method change value's type from string to double
     *
     * @param information              original list with values
     * @return                         new list with values
     * @throws ParseProcessException   if some one value isn't number
     */
    public List<Double> changeValueType(List<String> information) throws ParseProcessException {

        List<Double> newValue = new LinkedList<>();

        for (String data : information){

            newValue.add(parseNumber(data));

        }
        return newValue;
    }
}
