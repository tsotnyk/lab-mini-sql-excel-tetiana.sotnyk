package com.j2core.sts.minisqlexcel.dto;

import java.io.Serializable;

/**
 * Created by sts on 10/7/16.
 */

public class CellId implements Serializable {

    private static final long serialVersionUID = 42L;
    protected int rowId;
    protected int columnId;

    public CellId() {
    }

    public CellId(int rowId, int columnId) {

        this.rowId = rowId;
        this.columnId = columnId;

    }

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public int getColumnId() {
        return columnId;
    }

    public void setColumnId(int columnId) {
        this.columnId = columnId;
    }
}
