package com.j2core.sts.minisqlexcel;

import com.j2core.sts.minisqlexcel.dao.DBWorkerDAO;
import com.j2core.sts.minisqlexcel.datautil.DataParser;
import com.j2core.sts.minisqlexcel.datautil.UserInputParser;
import com.j2core.sts.minisqlexcel.datautil.VerifierData;
import com.j2core.sts.minisqlexcel.util.PropertyLoaderLazyLoadSingleton;
import com.j2core.sts.minisqlexcel.dao.WorkerDAO;
import com.j2core.sts.minisqlexcel.service.PerformerService;
import com.j2core.sts.minisqlexcel.service.DefaultPerformerService;
import org.apache.log4j.Logger;
import snaq.db.ConnectionPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 * Created by sts on 8/21/16.
 */
public class Main {

    private static final Logger LOGGER = Logger.getLogger(Main.class); // object for save log information
    private static final String DB_URL = "jdbc:mysql://localhost:3306/";
    private static final String NAME_DB = "testDataB";
    private static final String USER = "sts";
    private static final String PASS = "StsStsSts!2#";
    private static final String COLUMNS = "CREATE TABLE cells ( rowId INT NOT NULL," +
            "                                                           columnId INT NOT NULL," +
            "                                                           value TEXT," +
            "                                                           PRIMARY KEY (rowId, columnId))";
    private static final String FULL_DB_URL = DB_URL + NAME_DB;
    private static ConnectionPool connectionPool;

    @edu.umd.cs.findbugs.annotations.SuppressFBWarnings(value = {"DMI_CONSTANT_DB_PASSWORD","REC_CATCH_EXCEPTION"}, justification = "Exception detail hide")
    public static void main(String[] args) {

        try (Connection testConnection = DriverManager.getConnection(DB_URL, USER, PASS); Statement testStatement = testConnection.createStatement();
             Statement statement = testConnection.createStatement()) {

             connectionPool = new ConnectionPool("St", 10, 25, 100, FULL_DB_URL, USER, PASS);

            String sql = "CREATE DATABASE " + NAME_DB;
            testStatement.execute(sql);
            String useDB = "USE " + NAME_DB;
            testStatement.execute(useDB);

            testStatement.execute(COLUMNS);

            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 1, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(1, 2, '5')");
            statement.executeUpdate("INSERT INTO cells (rowId, columnId, value) VALUES(2, 1, '55')");

            DBWorkerDAO dbWorker = new WorkerDAO(connectionPool, PropertyLoaderLazyLoadSingleton.getInstance().getProperties());
            DataParser dataParser = new DataParser();
            VerifierData verifierData = new VerifierData(dataParser, dbWorker);
            UserInputParser userInputParser = new UserInputParser();
            PerformerService performerService = new DefaultPerformerService(dbWorker, userInputParser, dataParser, verifierData);
            UserInteraction service = new UserInteraction(performerService, userInputParser);

            service.doUserInteraction();

        } catch (Exception e) {
             LOGGER.error(e);
            System.out.println("Sorry! Some thing wrong! Please look the log file");
        } finally {
            try (Connection connection = connectionPool.getConnection(); Statement stmt = connection.createStatement()) {

                String sql = "DROP DATABASE " + NAME_DB;
                stmt.executeUpdate(sql);

            } catch (Exception e) {
                 LOGGER.error(e);
            }
        }
    }
}
