package com.j2core.sts.minisqlexcel.dao.exception;

/**
 * Created by sts on 8/21/16.
 */

/**
 * Class exception if not found id in DB
 */
public class NotFoundIdException extends Exception {

    public NotFoundIdException()  {
    }
}
